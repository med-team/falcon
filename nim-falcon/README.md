# nim-falcon
Nim versions of FALCON executables

See https://github.com/PacificBiosciences/FALCON

## Goal
* A single executable
* On Linux, a static binary
