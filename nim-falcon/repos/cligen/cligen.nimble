# Package
version     = "0.9.6"
author      = "Charles Blake"
description = "Infer & generate command-line interace/option/argument parser"
license     = "MIT/ISC"

# Deps
requires    "nim >= 0.13.0"
